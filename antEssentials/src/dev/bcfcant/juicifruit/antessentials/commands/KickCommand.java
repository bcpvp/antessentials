package dev.bcfcant.juicifruit.antessentials.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionDefault;

import dev.bcfcant.juicifruit.antessentials.AntEssentials;
import dev.bcfcant.juicifruit.antessentials.core.EssCommand;
import dev.bcfcant.juicifruit.antessentials.core.Permissions;
import dev.bcfcant.juicifruit.antessentials.core.Reference;
import dev.bcfcant.juicifruit.antessentials.core.player.User;
import dev.bcfcant.juicifruit.antlib.core.util.MsgUtil;

public class KickCommand extends EssCommand {

	public KickCommand(AntEssentials ess) {
		super(ess, "kick", "Kick the specified player", "ess.kick", PermissionDefault.OP);
		this.setUsage("/kick <player> [reason]");
	}

	@Override
	public boolean run(CommandSender sender, String label, String[] args) {
		if (args.length == 0) {
			MsgUtil.msg(sender, "Kick", ChatColor.RED + "You must specify a player to kick.");
			return true;
		}

		Player toKick = ess.getServer().getPlayer(args[0]);
		if (toKick == null) {
			MsgUtil.msg(sender, "Kick", ChatColor.GOLD + "Could not find player: " + ChatColor.RED + args[0]);
			return true;
		}

		User user = ess.getUser(toKick);

		if (user.hasPermission(Permissions.KICK_EXTEMPT)) {
			MsgUtil.msg(sender, "Kick", ChatColor.RED + user.getName() + ChatColor.GOLD + " is exempt from kicks.");
			return true;
		}

		String kickReason;
		if (args.length == 1) {
			kickReason = Reference.DEFAULT_KICK_REASON;
		} else {
			kickReason = ChatColor.translateAlternateColorCodes('&', getFinalArg(args, 1).replace("\\n", "\n").replace("|", "\n"));
		}

		user.getBase().kickPlayer(ChatColor.RED + kickReason);

		String senderName = sender instanceof Player ? sender.getName() : "Console";
		String kickNotify = ChatColor.RED + senderName + ChatColor.GOLD + " kicked " + ChatColor.RED + user.getName() + ChatColor.GOLD + " for " + ChatColor.RED + kickReason
				+ ChatColor.GOLD + ".";

		ess.notify(Permissions.KICK_NOTIFY, "Kick", kickNotify);
		return true;
	}

	private static String getFinalArg(String[] args, int start) {
		StringBuilder bldr = new StringBuilder();
		for (int i = start; i < args.length; i++) {
			if (i != start) {
				bldr.append(" ");
			}
			bldr.append(args[i]);
		}
		return bldr.toString();
	}
}
