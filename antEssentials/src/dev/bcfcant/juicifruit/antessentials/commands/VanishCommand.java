package dev.bcfcant.juicifruit.antessentials.commands;

import java.util.Arrays;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionDefault;

import dev.bcfcant.juicifruit.antessentials.AntEssentials;
import dev.bcfcant.juicifruit.antessentials.core.EssCommand;
import dev.bcfcant.juicifruit.antlib.api.VanishAPI;
import dev.bcfcant.juicifruit.antlib.core.util.MsgUtil;

public class VanishCommand extends EssCommand {

	public VanishCommand(AntEssentials ess) {
		super(ess, "vanish", "Toggles vanish for the player", "ess.vanish", PermissionDefault.OP);
		this.setAliases(Arrays.asList("v", "unvanish"));
		this.setUsage("/vanish [true/false]");
	}

	@Override
	public boolean run(CommandSender sender, String label, String[] args) {
		if (!isPlayer(sender)) {
			return true;
		}

		Player player = (Player) sender;

		if (label.equalsIgnoreCase("unvanish")) {
			VanishAPI.setVanished(player, false);
			MsgUtil.msg(player, ChatColor.DARK_AQUA + "Vanish", ChatColor.AQUA + (VanishAPI.isVanished(player) ? "Now vanished" : "No longer vanished"));
			return true;
		}

		if (args.length >= 1) {
			if (args[0].equalsIgnoreCase("true")) {
				VanishAPI.setVanished(player, true);
			} else if (args[0].equalsIgnoreCase("false")) {
				VanishAPI.setVanished(player, false);
			} else {
				VanishAPI.toggleVanish(player);
			}
		} else {
			VanishAPI.toggleVanish(player);
		}

		MsgUtil.msg(player, ChatColor.DARK_AQUA + "Vanish", ChatColor.AQUA + (VanishAPI.isVanished(player) ? "Now vanished" : "No longer vanished"));
		return true;
	}
}
