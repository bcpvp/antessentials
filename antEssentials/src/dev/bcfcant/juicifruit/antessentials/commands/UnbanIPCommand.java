package dev.bcfcant.juicifruit.antessentials.commands;

import org.bukkit.BanList;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionDefault;

import dev.bcfcant.juicifruit.antessentials.AntEssentials;
import dev.bcfcant.juicifruit.antessentials.core.EssCommand;
import dev.bcfcant.juicifruit.antessentials.core.Permissions;
import dev.bcfcant.juicifruit.antessentials.core.player.PlayerLoader;
import dev.bcfcant.juicifruit.antessentials.core.player.User;
import dev.bcfcant.juicifruit.antessentials.core.util.FormatUtil;
import dev.bcfcant.juicifruit.antlib.core.util.MsgUtil;

public class UnbanIPCommand extends EssCommand {

	public UnbanIPCommand(AntEssentials ess) {
		super(ess, "unbanip", "IP unban the specified player", "ess.unbanip", PermissionDefault.OP);
		this.setUsage("/unbanip <player/ip>");
	}

	@Override
	public boolean run(CommandSender sender, String label, String[] args) {
		if (args.length == 0) {
			sendUsageMessage(sender);
			return true;
		}

		String senderName = sender instanceof Player ? sender.getName() : "Console";

		String ipAddress = null;
		if (FormatUtil.validIP(args[0])) {
			ipAddress = args[0];
		} else {
			String name = ess.matchPlayer(args[0]);
			if (name != null) {
				Player player = ess.getServer().getPlayer(name);
				if (player != null) {
					ipAddress = player.getAddress().getAddress().getHostAddress();
				} else {
					OfflinePlayer op = ess.getServer().getOfflinePlayer(name);
					User user = ess.getUser(PlayerLoader.loadPlayer(op.getUniqueId()));
					ipAddress = user.getLastLoginAddress();
				}
			}
		}

		if (ipAddress == null) {
			MsgUtil.msg(sender, "Ban", ChatColor.RED + "Could not find player: " + args[0]);
			return true;
		}

		ess.getServer().getBanList(BanList.Type.IP).pardon(ipAddress);

		String banDisplay = ChatColor.RED + senderName + ChatColor.GOLD + " unbanned ip " + ChatColor.RED + ipAddress + ChatColor.GOLD + ".";

		ess.notify(Permissions.BAN_NOTIFY, "Ban", banDisplay);
		return true;
	}
}
