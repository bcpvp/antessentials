package dev.bcfcant.juicifruit.antessentials.commands;

import java.util.Arrays;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.permissions.PermissionDefault;

import dev.bcfcant.juicifruit.antessentials.AntEssentials;
import dev.bcfcant.juicifruit.antessentials.core.EssCommand;
import dev.bcfcant.juicifruit.antlib.core.util.MsgUtil;

public class GhostFixCommand extends EssCommand {

	public GhostFixCommand(AntEssentials ess) {
		super(ess, "ghostfix", "Attempts to de-ghost the player", "ess.ghostfix", PermissionDefault.TRUE);
		this.setAliases(Arrays.asList("gfix", "gf"));
	}

	@Override
	public boolean run(CommandSender sender, String label, String[] args) {
		if (!isPlayer(sender)) {
			return true;
		}

		Player player = (Player) sender;
		player.teleport(player.getLocation(), TeleportCause.COMMAND);
		MsgUtil.msg(sender, ChatColor.GOLD + "The server has attempted to de-ghost you.");

		return true;
	}
}
