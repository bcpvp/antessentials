package dev.bcfcant.juicifruit.antessentials.commands;

import dev.bcfcant.juicifruit.antessentials.AntEssentials;
import dev.bcfcant.juicifruit.antessentials.core.EssCommand;
import dev.bcfcant.juicifruit.antlib.api.DisguiseAPI;
import dev.bcfcant.juicifruit.antlib.api.Ranks;
import dev.bcfcant.juicifruit.antlib.core.util.MsgUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionDefault;

import java.util.Arrays;

public class WhoisCommand extends EssCommand {
    public WhoisCommand(AntEssentials ess) {
        super(ess, "whois", "Finds a disguised player's real name and rank.", "ess.whois", PermissionDefault.OP);
        setUsage("/whois <diguiseName>");
        setAliases(Arrays.asList("who"));
    }

    @Override
    public boolean run(CommandSender sender, String label, String[] args) {
        if (args.length == 0) {
            sendUsageMessage(sender);
        }

        String name = args[0];

        String realName = null;
        Ranks rank = Ranks.REGULAR;

        for (Player player : Bukkit.getOnlinePlayers()) {
            if (!DisguiseAPI.isDisguised(player))
                continue;

            if (ChatColor.stripColor(DisguiseAPI.getDisguise(player)).equalsIgnoreCase(name)) {
                realName = player.getName();
                rank = Ranks.getRank(player);
                break;
            }
        }

        if (realName == null) {
            MsgUtil.msg(sender, "WhoIs", "§c" + name + " is either not a disguised name, or is offline.");
            return true;
        }

        MsgUtil.msg(sender, "WhoIs", "§6" + name + "'s real name is §c" + realName + " (rank " + rank.getName() + "§c)§6.");
        return true;
    }
}
