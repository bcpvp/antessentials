package dev.bcfcant.juicifruit.antessentials.commands;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.bukkit.BanEntry;
import org.bukkit.BanList;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionDefault;

import dev.bcfcant.juicifruit.antessentials.AntEssentials;
import dev.bcfcant.juicifruit.antessentials.core.EssCommand;
import dev.bcfcant.juicifruit.antessentials.core.player.PlayerLoader;
import dev.bcfcant.juicifruit.antessentials.core.player.User;
import dev.bcfcant.juicifruit.antessentials.core.util.DateUtil;
import dev.bcfcant.juicifruit.antlib.api.DisguiseAPI;
import dev.bcfcant.juicifruit.antlib.api.analytics.AnalyticElement;
import dev.bcfcant.juicifruit.antlib.api.analytics.AnalyticManager;
import dev.bcfcant.juicifruit.antlib.core.util.MsgUtil;

public class AnalyseCommand extends EssCommand {
	public AnalyseCommand(AntEssentials ess) {
		super(ess, "analyse", "Executes an analyse on server data.", "ess.analyse", PermissionDefault.OP);
	}

	@Override
	public boolean run(CommandSender sender, String label, String[] args) {

		sender.sendMessage(ChatColor.GOLD + ChatColor.STRIKETHROUGH.toString() + "==               =" + ChatColor.RESET + ChatColor.GOLD + " Analysis report "
				+ ChatColor.STRIKETHROUGH + "=               ==");

		if (args.length == 1) {
			String pName = ess.matchPlayer(args[0]);
			Player player = null;

			if (pName == null) {
				for (Player p : ess.getServer().getOnlinePlayers()) {
					if (p.getPlayerListName().equalsIgnoreCase(args[0]))
						pName = p.getName();
					else if (p.getDisplayName().equalsIgnoreCase(args[0]))
						pName = p.getName();
					else if (DisguiseAPI.getDisguise(p).equalsIgnoreCase(args[0]))
						pName = p.getName();
				}
			}

			if (pName != null) {
				player = PlayerLoader.loadPlayer(pName);
			}

			if (player == null) {
				MsgUtil.msg(sender, ChatColor.DARK_AQUA + "Analysis", ChatColor.RED + "Unknown player: " + args[0]);
				return true;
			}

			User user = ess.getUser(player);
			List<String> info = new ArrayList<String>();

			Location loc = user.getLocation();
			info.add(ChatColor.DARK_AQUA + "Name" + ChatColor.DARK_GRAY + ": " + ChatColor.AQUA + player.getName());
			info.add(ChatColor.DARK_AQUA + "Location" + ChatColor.DARK_GRAY + ": " + ChatColor.AQUA + "x: " + loc.getBlockX() + ", y: " + loc.getBlockY() + ", z: "
					+ loc.getBlockZ());
			info.add(ChatColor.DARK_AQUA + "Gamemode" + ChatColor.DARK_GRAY + ": " + ChatColor.AQUA + player.getGameMode().toString().toLowerCase());
			info.add(ChatColor.DARK_AQUA + (player.isOnline() ? "IP" : "Last IP") + ChatColor.DARK_GRAY + ": " + ChatColor.AQUA
					+ (player.isOnline() ? player.getAddress().getAddress().getHostAddress() : user.getLastLoginAddress()));

			BanList bans = ess.getServer().getBanList(BanList.Type.NAME);
			BanEntry ban = bans.getBanEntry(player.getName());

			if (ban == null) {
				BanList bans2 = ess.getServer().getBanList(BanList.Type.IP);
				ban = bans2.getBanEntry(player.isOnline() ? player.getAddress().getAddress().getHostAddress() : user.getLastLoginAddress());
			}

			if (ban != null) {
				info.add(ChatColor.DARK_AQUA
						+ "Ban"
						+ ChatColor.DARK_GRAY
						+ ": "
						+ ChatColor.AQUA
						+ "[Reason: "
						+ ban.getReason()
						+ ChatColor.AQUA
						+ "]"
						+ (ban.getExpiration() == null ? ""
								: " [Expires: "
										+ DateUtil.formatDateDiff(Calendar.getInstance(), new GregorianCalendar(ban.getExpiration().getYear(), ban.getExpiration().getMonth(), ban.getExpiration().getDate(), ban.getExpiration().getHours(), ban.getExpiration().getMinutes(), ban.getExpiration().getSeconds())))
						+ "]");
			}

			for (String line : info)
				sender.sendMessage(line);

			return true;
		}

		for (AnalyticElement elements : AnalyticManager.getInstance().getElements()) {
			Object rawValue = elements.getValue();
			String formattedValue = "???";
			String color = ChatColor.AQUA.toString();

			if (rawValue instanceof Boolean) {
				boolean value = (Boolean) rawValue;
				color = value ? ChatColor.GREEN.toString() : ChatColor.RED.toString();
				formattedValue = value ? "Yes" : "No";
			} else if (rawValue instanceof Integer) {
				formattedValue = "" + rawValue;
			} else if (rawValue instanceof String) {
				formattedValue = String.valueOf(rawValue);
			} else if (rawValue instanceof Long) {
				formattedValue = "" + rawValue;
			} else if (rawValue instanceof Double) {
				formattedValue = "" + rawValue;
			} else if (rawValue instanceof Float) {
				formattedValue = "" + rawValue;
			}

			sender.sendMessage(ChatColor.DARK_AQUA + elements.getDisplayName() + ChatColor.DARK_GRAY + ": " + ChatColor.RESET + color + formattedValue);
		}

		return true;
	}
}
