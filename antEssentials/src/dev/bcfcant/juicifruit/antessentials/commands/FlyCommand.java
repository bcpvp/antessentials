package dev.bcfcant.juicifruit.antessentials.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionDefault;

import dev.bcfcant.juicifruit.antessentials.AntEssentials;
import dev.bcfcant.juicifruit.antessentials.core.EssCommand;
import dev.bcfcant.juicifruit.antlib.core.util.MsgUtil;

public class FlyCommand extends EssCommand {

	public FlyCommand(AntEssentials ess) {
		super(ess, "fly", "Toggles flight the player", "ess.fly", PermissionDefault.OP);
		this.setUsage("/fly [true/false]");
	}

	@Override
	public boolean run(CommandSender sender, String label, String[] args) {
		if (!isPlayer(sender)) {
			return true;
		}

		Player player = (Player) sender;

		if (args.length >= 1) {
			if (args[0].equalsIgnoreCase("true")) {
				player.setAllowFlight(true);
			} else if (args[0].equalsIgnoreCase("false")) {
				player.setAllowFlight(false);
			} else {
				player.setAllowFlight(!player.getAllowFlight());
			}
		} else {
			player.setAllowFlight(!player.getAllowFlight());
		}

		if (!player.getAllowFlight()) {
			player.setFlying(false);
		}

		MsgUtil.msg(player, "Fly", ChatColor.AQUA + "Flight: " + ChatColor.RED + (player.getAllowFlight() ? "enabled" : "disabled"));
		return true;
	}
}
