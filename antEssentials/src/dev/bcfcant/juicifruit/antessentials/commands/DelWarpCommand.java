package dev.bcfcant.juicifruit.antessentials.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionDefault;

import dev.bcfcant.juicifruit.antessentials.AntEssentials;
import dev.bcfcant.juicifruit.antessentials.core.EssCommand;
import dev.bcfcant.juicifruit.antlib.core.util.MsgUtil;

public class DelWarpCommand extends EssCommand {

	public DelWarpCommand(AntEssentials ess) {
		super(ess, "delwarp", "Deletes the warp with the specified name", "ess.delwarp", PermissionDefault.OP);
		this.setUsage("/delwarp <name>");
	}

	@Override
	public boolean run(CommandSender sender, String label, String[] args) {
		if (!isPlayer(sender)) {
			return true;
		}

		Player player = (Player) sender;

		if (args.length == 0) {
			sendUsageMessage(player);
			return true;
		}

		boolean success = ess.getWarps().delWarp(args[0]);
		if (success) {
			MsgUtil.msg(player, "Warp", ChatColor.GOLD + "Set warp " + ChatColor.RED + args[0]);
		} else {
			MsgUtil.msg(player, "Warp", ChatColor.RED + "Could not find warp: " + args[0]);
		}

		return true;
	}
}
