package dev.bcfcant.juicifruit.antessentials.commands;

import java.util.Arrays;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionDefault;

import dev.bcfcant.juicifruit.antessentials.AntEssentials;
import dev.bcfcant.juicifruit.antessentials.core.EssCommand;
import dev.bcfcant.juicifruit.antessentials.core.Permissions;
import dev.bcfcant.juicifruit.antessentials.core.player.User;
import dev.bcfcant.juicifruit.antessentials.core.util.DateUtil;
import dev.bcfcant.juicifruit.antlib.core.util.MsgUtil;

public class MuteCommand extends EssCommand {

	public MuteCommand(AntEssentials ess) {
		super(ess, "mute", "Mutes the specified player allowing the user to set a timeout", "ess.mute", PermissionDefault.OP);
		this.setAliases(Arrays.asList("unmute", "silence", "unsilence"));
		this.setUsage("/mute <player> [time]");
	}

	@Override
	public boolean run(CommandSender sender, String label, String[] args) {
		if (args.length == 0) {
			sendUsageMessage(sender);
			return true;
		}

		Player toMute = ess.getServer().getPlayer(args[0]);
		if (toMute == null) {
			MsgUtil.msg(sender, "Chat", ChatColor.RED + "Could not find player: " + args[0]);
			return true;
		}

		User user = ess.getUser(toMute);

		if (user.hasPermission(Permissions.MUTE_EXTEMPT)) {
			MsgUtil.msg(sender, "Chat", ChatColor.RED + user.getName() + ChatColor.GOLD + " is exempt from mutes.");
			return true;
		}

		if (label.toLowerCase().startsWith("un")) {
			user.setMuteTimeout(0L);
			user.sendUtilMessage("Chat", ChatColor.GOLD + "You have been unmuted.");
			user.setMuted(false);

			MsgUtil.msg(sender, "Chat", ChatColor.RED + "Unmuted " + ChatColor.RED + user.getName() + ChatColor.GOLD + ".");

			String senderName = sender instanceof Player ? sender.getName() : "Console";
			String muteNotify = ChatColor.RED + senderName + ChatColor.GOLD + " unmuted " + ChatColor.RED + user.getName() + ChatColor.GOLD + ".";

			ess.notify(Permissions.MUTE_NOTIFY, "Chat", muteNotify);
			return true;
		} else {
			long muteTimestamp = 0L;

			if (args.length > 1) {
				String time = getFinalArg(args, 1);
				try {
					muteTimestamp = DateUtil.parseDateDiff(time, true);
				} catch (Exception e) {
					MsgUtil.msg(sender, "Chat", "Invalid mute time.");
				}
				user.setMuted(true);
			} else {
				user.setMuted(!user.isMuted());
			}
			user.setMuteTimeout(muteTimestamp);
			boolean muted = user.isMuted();
			String muteTime = DateUtil.formatDateDiff(muteTimestamp);

			if (muted) {
				if (muteTimestamp > 0L) {
					MsgUtil.msg(sender, "Chat", ChatColor.GOLD + "Muted " + ChatColor.RED + user.getDisplayName() + ChatColor.GOLD + " for " + ChatColor.RED + muteTime
							+ ChatColor.GOLD + ".");
					user.sendUtilMessage("Chat", ChatColor.GOLD + "You have been muted for " + ChatColor.RED + muteTime + ChatColor.GOLD + ".");
				} else {
					MsgUtil.msg(sender, "Chat", ChatColor.GOLD + "Muted " + ChatColor.RED + user.getDisplayName() + ChatColor.GOLD + ".");
					user.sendUtilMessage("Chat", ChatColor.GOLD + "You have been muted.");
				}
			} else {
				MsgUtil.msg(sender, "Chat", ChatColor.RED + "Unmuted " + ChatColor.RED + user.getName() + ChatColor.GOLD + ".");
				user.sendUtilMessage("Chat", ChatColor.GOLD + "You have been unmuted.");
			}

			String senderName = sender instanceof Player ? sender.getName() : "Console";
			String muteNotify = ChatColor.RED + senderName + ChatColor.GOLD + " " + (muted ? "muted" : "unmuted") + " " + ChatColor.RED + user.getName() + ChatColor.GOLD
					+ (muteTimestamp > 0L ? " for " + ChatColor.RED + muteTime + ChatColor.GOLD + "." : ".");

			ess.notify(Permissions.MUTE_NOTIFY, "Chat", muteNotify);
		}
		return true;
	}

	private static String getFinalArg(String[] args, int start) {
		StringBuilder bldr = new StringBuilder();
		for (int i = start; i < args.length; i++) {
			if (i != start) {
				bldr.append(" ");
			}
			bldr.append(args[i]);
		}
		return bldr.toString();
	}
}
