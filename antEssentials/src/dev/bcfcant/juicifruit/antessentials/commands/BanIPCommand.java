package dev.bcfcant.juicifruit.antessentials.commands;

import org.bukkit.BanList;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionDefault;

import dev.bcfcant.juicifruit.antessentials.AntEssentials;
import dev.bcfcant.juicifruit.antessentials.core.EssCommand;
import dev.bcfcant.juicifruit.antessentials.core.Permissions;
import dev.bcfcant.juicifruit.antessentials.core.Reference;
import dev.bcfcant.juicifruit.antessentials.core.player.PlayerLoader;
import dev.bcfcant.juicifruit.antessentials.core.player.User;
import dev.bcfcant.juicifruit.antessentials.core.util.FormatUtil;
import dev.bcfcant.juicifruit.antlib.core.util.MsgUtil;

public class BanIPCommand extends EssCommand {

	public BanIPCommand(AntEssentials ess) {
		super(ess, "banip", "IP ban the specified player", "ess.banip", PermissionDefault.OP);
		this.setUsage("/banip <player/ip> [reason]");
	}

	@Override
	public boolean run(CommandSender sender, String label, String[] args) {
		if (args.length == 0) {
			sendUsageMessage(sender);
			return true;
		}

		String senderName = sender instanceof Player ? sender.getName() : "Console";

		String ipAddress = null;
		if (FormatUtil.validIP(args[0])) {
			ipAddress = args[0];
		} else {
			String name = ess.matchPlayer(args[0]);
			if (name != null) {
				Player player = ess.getServer().getPlayer(name);
				if (player != null) {
					ipAddress = player.getAddress().getAddress().getHostAddress();
				} else {
					OfflinePlayer op = ess.getServer().getOfflinePlayer(name);
					User user = ess.getUser(PlayerLoader.loadPlayer(op.getUniqueId()));
					ipAddress = user.getLastLoginAddress();
				}
			}
		}

		if (ipAddress == null) {
			MsgUtil.msg(sender, "Ban", ChatColor.RED + "Could not find player: " + args[0]);
			return true;
		}

		String banReason;
		if (args.length == 1) {
			banReason = Reference.DEFAULT_BAN_REASON;
		} else {
			banReason = ChatColor.translateAlternateColorCodes('&', getFinalArg(args, 1).replace("\\n", "\n").replace("|", "\n"));
		}

		ess.getServer().getBanList(BanList.Type.IP).addBan(ipAddress, banReason, null, senderName);
		for (Player player : ess.getServer().getOnlinePlayers()) {
			if (player.getAddress().getAddress().getHostAddress().equals(ipAddress)) {
				player.kickPlayer(ChatColor.DARK_RED + "Banned\n\n" + ChatColor.RED + banReason);
			}
		}

		String banDisplay = ChatColor.RED + senderName + ChatColor.GOLD + " banned ip " + ChatColor.RED + ipAddress + ChatColor.GOLD + " for " + ".";

		ess.notify(Permissions.BAN_NOTIFY, "Ban", banDisplay);
		return true;
	}

	private static String getFinalArg(String[] args, int start) {
		StringBuilder bldr = new StringBuilder();
		for (int i = start; i < args.length; i++) {
			if (i != start) {
				bldr.append(" ");
			}
			bldr.append(args[i]);
		}
		return bldr.toString();
	}
}
