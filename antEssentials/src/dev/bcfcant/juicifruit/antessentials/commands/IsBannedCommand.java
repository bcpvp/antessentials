package dev.bcfcant.juicifruit.antessentials.commands;

import dev.bcfcant.juicifruit.antessentials.AntEssentials;
import dev.bcfcant.juicifruit.antessentials.api.UUIDLookup;
import dev.bcfcant.juicifruit.antessentials.core.EssCommand;
import dev.bcfcant.juicifruit.antlib.core.util.MsgUtil;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.permissions.PermissionDefault;

import java.util.Arrays;
import java.util.UUID;

/*
 * This is really broken :)
 */
public class IsBannedCommand extends EssCommand {
    public IsBannedCommand(AntEssentials ess) {
        super(ess, "isbanned", "Looks up if a player is banned", "ess.isbanned", PermissionDefault.OP);
        setAliases(Arrays.asList("banned"));
        setUsage("/isbanned <uuid:name> <playerUUID:playerName>.");
    }

    @Override
    public boolean run(CommandSender sender, String label, String[] args) {
        if (args.length < 2) {
            MsgUtil.msg(sender, "Ban Lookup", "§cPlease specify whether you are using a name or an UUID, and then enter the player's name/UUID.");
            return true;
        }

        String type = args[0];
        String value = args[1];

        if (type.equalsIgnoreCase("name")) {

            try {
                UUID uuid = UUIDLookup.getUUIDOf(value);
                if (isBanned(uuid)) {
                    MsgUtil.msg(sender, "Ban Lookup", "§6" + value + " is §cbanned§6.");
                } else {
                    MsgUtil.msg(sender, "Ban Lookup", "§6" + value + " is §anot banned§6.");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (type.equalsIgnoreCase("uuid")) {
            try {
                UUID uuid = UUID.fromString(value);

                if (isBanned(uuid)) {
                    MsgUtil.msg(sender, "Ban Lookup", "§6" + value + " is §cbanned§6.");
                } else {
                    MsgUtil.msg(sender, "Ban Lookup", "§6" + value + " is §anot banned§6.");
                }
            } catch (Exception ex) {
                MsgUtil.msg(sender, "Ban Lookup", "§c" + value + " is not a valid Unique ID (UUIDv4).");
            }
        } else {
            MsgUtil.msg(sender, "Ban Lookup", "§c" + type + " is not a recognised entry type. Please use \"name\" or \"uuid\".");
        }

        return true;
    }

    private boolean isBanned(UUID uuid) {
        OfflinePlayer player = Bukkit.getOfflinePlayer(uuid);
        if (player == null)
            return false;

        return player.isBanned();
    }
}
