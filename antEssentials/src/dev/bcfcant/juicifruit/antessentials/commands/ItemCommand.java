package dev.bcfcant.juicifruit.antessentials.commands;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.permissions.PermissionDefault;

import com.google.common.base.Joiner;

import dev.bcfcant.juicifruit.antessentials.AntEssentials;
import dev.bcfcant.juicifruit.antessentials.core.EssCommand;
import dev.bcfcant.juicifruit.antlib.api.item.ItemInfo;
import dev.bcfcant.juicifruit.antlib.api.item.Items;
import dev.bcfcant.juicifruit.antlib.core.util.MsgUtil;

public class ItemCommand extends EssCommand {

	public ItemCommand(AntEssentials ess) {
		super(ess, "item", "Gives the player items", "ess.item", PermissionDefault.OP);
		this.setAliases(Arrays.asList("i"));
		this.setUsage("/item <item>[:data] [amount] [dataTag]");
	}

	@Override
	public boolean run(CommandSender sender, String label, String[] args) {
		if (!isPlayer(sender)) {
			return true;
		}

		if (args.length < 1) {
			sendUsageMessage(sender);
			return true;
		}

		Player player = (Player) sender;

		String[] mdTag = args[0].split(":");
		String materialTag = mdTag[0];

		Material material = Material.matchMaterial(materialTag);

		if (material == null) {
			material = Bukkit.getUnsafe().getMaterialFromInternalName(materialTag);
		}

		if (material == null) {
			ItemInfo info = Items.itemByName(materialTag);

			if (info == null) {
				info = Items.itemByString(args[0]);
			}

			if (info != null) {
				material = info.getType();
			}
		}

		if (material == null) {
			for (Material mat : Material.values()) {
				if (mat.toString().replace("_", "").equalsIgnoreCase(materialTag))
					material = mat;
			}
		}

		if (material != null) {
			int amount = 1;
			short data = 0;

			if (mdTag.length >= 2) {
				try {
					data = Short.parseShort(mdTag[1]);
				} catch (NumberFormatException ex) {
				}
			}

			if (args.length >= 2) {
				amount = 1;

				try {
					amount = Integer.parseInt(args[1]);
				} catch (NumberFormatException e) {
				}

				if (amount < 1)
					amount = 1;
				if (amount > 64)
					amount = 64;
			}

			ItemStack stack = new ItemStack(material, amount, data);

			if (args.length >= 3) {
				try {
					stack = Bukkit.getUnsafe().modifyItemStack(stack, Joiner.on(' ').join(Arrays.asList(args).subList(2, args.length)));
				} catch (Throwable t) {
					player.sendMessage("Not a valid tag");
					return true;
				}
			}

			player.getInventory().addItem(stack);

			MsgUtil.msg(sender, "Give", ChatColor.GOLD + "Given you " + ChatColor.RED + amount + " " + Items.itemByType(material, data).getName());
		} else {
			MsgUtil.msg(sender, "Give", ChatColor.RED + "There's no item called " + args[0]);
		}

		return true;
	}
}
