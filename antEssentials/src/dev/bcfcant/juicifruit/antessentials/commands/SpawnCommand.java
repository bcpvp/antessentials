package dev.bcfcant.juicifruit.antessentials.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.permissions.PermissionDefault;

import dev.bcfcant.juicifruit.antessentials.AntEssentials;
import dev.bcfcant.juicifruit.antessentials.core.EssCommand;
import dev.bcfcant.juicifruit.antlib.core.util.MsgUtil;

public class SpawnCommand extends EssCommand {

	public SpawnCommand(AntEssentials ess) {
		super(ess, "spawn", "Teleports you to spawn", "ess.spawn", PermissionDefault.TRUE);
		setUsage("/spawn [player]");
	}

	@Override
	public boolean run(CommandSender commandSender, String label, String[] args) {
		if (commandSender instanceof Player) {
			Player player = (Player) commandSender;

			if (args.length == 0) {
				player.teleport(ess.getServer().getWorlds().get(0).getSpawnLocation(), TeleportCause.COMMAND);
				return true;
			} else {
				if (player.hasPermission("ess.spawn.others")) {
					String targetN = args[0];
					Player target = Bukkit.getPlayer(targetN);

					if (target == null) {
						MsgUtil.msg(player, "Spawn", ChatColor.RED + "Unable to find player: " + targetN);
						return true;
					}

					MsgUtil.msg(player, "Spawn", ChatColor.GOLD + "You were teleported to spawn.");
					target.teleport(ess.getServer().getWorlds().get(0).getSpawnLocation(), TeleportCause.COMMAND);
					return true;
				} else {
					MsgUtil.msg(player, "Spawn", ChatColor.RED + "You do not have permission to teleport other players.");
					return true;
				}
			}
		}

		return true;
	}
}
