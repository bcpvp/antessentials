package dev.bcfcant.juicifruit.antessentials.commands;

import java.util.Arrays;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionDefault;

import dev.bcfcant.juicifruit.antessentials.AntEssentials;
import dev.bcfcant.juicifruit.antessentials.core.EssCommand;
import dev.bcfcant.juicifruit.antlib.api.DisguiseAPI;
import dev.bcfcant.juicifruit.antlib.api.Ranks;

public class ListCommand extends EssCommand {

	public ListCommand(AntEssentials ess) {
		super(ess, "list", "Lists the players on the server", "ess.list", PermissionDefault.TRUE);
		this.setAliases(Arrays.asList("plist"));
	}

	@Override
	public boolean run(CommandSender sender, String label, String[] args) {
		sender.sendMessage(ChatColor.GOLD + "There are " + ChatColor.DARK_GRAY + "[" + ChatColor.DARK_AQUA + ess.getServer().getOnlinePlayers().length + "/" + ChatColor.DARK_AQUA
				+ ess.getServer().getMaxPlayers() + ChatColor.DARK_GRAY + "]" + ChatColor.GOLD + " players online.");

		sender.sendMessage(ChatColor.GOLD + "Players:");

		StringBuilder sb = new StringBuilder();
		for (Player player : ess.getServer().getOnlinePlayers()) {
			if (sender instanceof Player && !((Player) sender).canSee(player))
				continue;
			if (sb.length() > 0)
				sb.append(ChatColor.GOLD).append(", ");
			sb.append((DisguiseAPI.isDisguised(player) ? Ranks.REGULAR.getTabColor() + ChatColor.stripColor(DisguiseAPI.getDisguise(player)) : Ranks.getRank(player).getTabColor()
					+ player.getName()));
		}
		sb.append(ChatColor.GOLD).append(".");
		sender.sendMessage(sb.toString());

		return true;
	}
}
