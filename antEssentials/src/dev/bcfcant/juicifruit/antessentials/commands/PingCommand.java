package dev.bcfcant.juicifruit.antessentials.commands;

import java.lang.reflect.Field;
import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionDefault;

import com.comphenix.protocol.injector.BukkitUnwrapper;

import dev.bcfcant.juicifruit.antessentials.AntEssentials;
import dev.bcfcant.juicifruit.antessentials.core.EssCommand;
import dev.bcfcant.juicifruit.antlib.core.util.MsgUtil;

public class PingCommand extends EssCommand {

	public PingCommand(AntEssentials ess) {
		super(ess, "ping", "Check a player's ping", "ess.ping", PermissionDefault.TRUE);
		this.setAliases(Arrays.asList("connection"));
		this.setUsage("/ping [player]");
	}

	@Override
	public boolean run(CommandSender sender, String label, String[] args) {
		if (args.length == 0) {
			if (!(sender instanceof Player)) {
				MsgUtil.msg(sender, "Ping", ChatColor.RED + "/" + label + " <player>");
				return true;
			}

			Player player = (Player) sender;
			int ping = getPing(player);

			if (ping == -1) {
				MsgUtil.msg(player, "Ping", ChatColor.GOLD + "Could not determine your ping.");
				return true;
			}

			MsgUtil.msg(player, "Ping", ChatColor.GOLD + "Your ping is " + ChatColor.AQUA + ping + "ms");
		} else {
			Player player = Bukkit.getServer().getPlayer(args[0]);
			if (player == null) {
				MsgUtil.msg(sender, "Ping", ChatColor.RED + "Could not find player: " + args[0]);
				return true;
			}

			int ping = getPing(player);

			if (ping == -1) {
				MsgUtil.msg(player, "Ping", ChatColor.GOLD + "Could not determine " + ChatColor.RED + player.getName() + ChatColor.GOLD + "'s ping.");
				return true;
			}

			MsgUtil.msg(sender, "Ping", ChatColor.RED + player.getName() + ChatColor.GOLD + "'s ping is " + ChatColor.AQUA + ping + "ms");
		}

		return true;
	}

	private int getPing(Player player) {
		int ping = -1;

		Object nmsPlayer = BukkitUnwrapper.getInstance().unwrapItem(player);
		try {
			Field f_ping = nmsPlayer.getClass().getField("ping");
			ping = f_ping.getInt(nmsPlayer);
		} catch (SecurityException e) {
		} catch (NoSuchFieldException e) {
		} catch (IllegalArgumentException e) {
		} catch (IllegalAccessException e) {
		}

		if (ping != -1)
			ping /= 2;

		return ping;
	}
}
