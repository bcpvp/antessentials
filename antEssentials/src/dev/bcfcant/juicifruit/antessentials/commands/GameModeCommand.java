package dev.bcfcant.juicifruit.antessentials.commands;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionDefault;

import dev.bcfcant.juicifruit.antessentials.AntEssentials;
import dev.bcfcant.juicifruit.antessentials.core.EssCommand;
import dev.bcfcant.juicifruit.antlib.core.util.MsgUtil;

public class GameModeCommand extends EssCommand {

	public GameModeCommand(AntEssentials ess) {
		super(ess, "gamemode", "Changes the player's gamemode", "ess.gamemode", PermissionDefault.OP);
		this.setAliases(Arrays.asList("gmode", "gm"));
		this.setUsage("/gamemode <mode> [player]");
	}

	@Override
	public boolean run(CommandSender sender, String label, String[] args) {
		Player player = null;

		if (args.length == 1) {
			if (!(sender instanceof Player)) {
				sender.sendMessage(ChatColor.RED + "Usage: /gamemode <mode> <player>");
				return true;
			}
			player = (Player) sender;
		} else if (args.length == 2) {
			if (!(sender.hasPermission("ess.gamemode.others"))) {
				MsgUtil.msg(sender, "Gamemode", ChatColor.RED + "Cannot set other player's gamemodes");
				return true;
			}
			player = Bukkit.getPlayer(args[1]);
			if (player == null) {
				MsgUtil.msg(sender, "Gamemode", ChatColor.RED + "Could not find player: " + args[1]);
				return true;
			}
		} else {
			sendUsageMessage(sender);
			return true;
		}

		int value = -1;

		try {
			value = Integer.parseInt(args[0]);
		} catch (NumberFormatException ex) {
		}

		GameMode mode = GameMode.getByValue(value);

		if (mode == null) {
			if (args[0].equalsIgnoreCase("creative") || args[0].equalsIgnoreCase("c")) {
				mode = GameMode.CREATIVE;
			} else if (args[0].equalsIgnoreCase("adventure") || args[0].equalsIgnoreCase("a")) {
				mode = GameMode.ADVENTURE;
			} else {
				mode = GameMode.SURVIVAL;
			}
		}

		player.setGameMode(mode);
		if (player.getGameMode().equals(mode)) {
			MsgUtil.msg(sender, "Gamemode", ChatColor.GOLD + "Set " + ChatColor.RED + player.getName() + ChatColor.GOLD + "'s gamemode to " + ChatColor.RED
					+ mode.toString().toLowerCase());
		} else {
			MsgUtil.msg(sender, "Gamemode", ChatColor.RED + "An unknown error occurred");
		}

		return true;
	}
}
