package dev.bcfcant.juicifruit.antessentials.commands;

import java.util.Arrays;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.permissions.PermissionDefault;

import dev.bcfcant.juicifruit.antessentials.AntEssentials;
import dev.bcfcant.juicifruit.antessentials.api.Warp;
import dev.bcfcant.juicifruit.antessentials.core.EssCommand;
import dev.bcfcant.juicifruit.antessentials.core.Permissions;
import dev.bcfcant.juicifruit.antlib.core.util.MsgUtil;

public class WarpCommand extends EssCommand {

	public WarpCommand(AntEssentials ess) {
		super(ess, "warp", "Teleports the player to the specified warp", "ess.warp", PermissionDefault.TRUE);
		this.setAliases(Arrays.asList("warps"));
		this.setUsage("/warp [warp] [player]");
	}

	@Override
	public boolean run(CommandSender sender, String label, String[] args) {
		if (!isPlayer(sender)) {
			return true;
		}

		Player player = (Player) sender;

		if (label.equalsIgnoreCase("warps") || args.length == 0) {
			player.sendMessage(ChatColor.GOLD + "Warps:");
			StringBuilder sb = new StringBuilder();
			for (String name : ess.getWarps().getWarps().keySet()) {
				if (sb.length() > 0)
					sb.append(ChatColor.GOLD).append(", ");
				sb.append(ChatColor.YELLOW).append(name);
			}
			player.sendMessage(sb.toString());
			return true;
		}

		Warp warp = ess.getWarps().getWarp(args[0]);
		if (warp == null) {
			MsgUtil.msg(player, "Warp", ChatColor.RED + "Could not find warp: " + args[0]);
			return true;
		}

		boolean success = false;
		if (args.length == 2 && player.hasPermission(Permissions.WARP_OTHER)) {
			Player other = ess.getServer().getPlayer(args[1]);
			if (other != null) {
				success = other.teleport(warp.getLocation(), TeleportCause.COMMAND);
				if (success) {
					MsgUtil.msg(player, "Warp", ChatColor.GOLD + "Warping " + other.getDisplayName() + ChatColor.GOLD + " to " + ChatColor.RED + warp.getName() + ChatColor.GOLD
							+ ".");
					MsgUtil.msg(other, "Warp", ChatColor.GOLD + "Warping to " + ChatColor.RED + warp.getName() + ChatColor.GOLD + ".");
				} else {
					MsgUtil.msg(player, "Warp", other.getDisplayName() + ChatColor.RED + "'s warp was prevented.");
				}
				return true;
			}
		}

		success = player.teleport(warp.getLocation(), TeleportCause.COMMAND);
		if (success) {
			MsgUtil.msg(player, "Warp", ChatColor.GOLD + "Warping to " + ChatColor.RED + warp.getName() + ChatColor.GOLD + ".");
		} else {
			MsgUtil.msg(player, "Warp", ChatColor.RED + "Your warp was prevented.");
		}

		return true;
	}
}
