package dev.bcfcant.juicifruit.antessentials.commands;

import java.util.Arrays;

import org.bukkit.BanList;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionDefault;

import dev.bcfcant.juicifruit.antessentials.AntEssentials;
import dev.bcfcant.juicifruit.antessentials.core.EssCommand;
import dev.bcfcant.juicifruit.antessentials.core.Permissions;
import dev.bcfcant.juicifruit.antlib.core.util.MsgUtil;

public class UnbanCommand extends EssCommand {

	public UnbanCommand(AntEssentials ess) {
		super(ess, "unban", "Unban the specified player", "ess.unban", PermissionDefault.OP);
		this.setAliases(Arrays.asList("pardon"));
		this.setUsage("/unban <player>");
	}

	@Override
	public boolean run(CommandSender sender, String label, String[] args) {
		if (args.length == 0) {
			sendUsageMessage(sender);
			return true;
		}

		String name = ess.matchPlayer(args[0]);
		if (name == null) {
			MsgUtil.msg(sender, "Ban", ChatColor.RED + "Could not find player: " + args[0]);
			return true;
		}

		ess.getServer().getBanList(BanList.Type.NAME).pardon(name);

		String senderName = sender instanceof Player ? sender.getName() : "Console";
		String unbanNotify = ChatColor.RED + senderName + ChatColor.GOLD + " unbanned " + ChatColor.RED + name + ChatColor.GOLD + ".";

		ess.notify(Permissions.BAN_NOTIFY, "Ban", unbanNotify);
		return true;
	}
}
