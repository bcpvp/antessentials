package dev.bcfcant.juicifruit.antessentials.commands;

import java.util.Arrays;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.permissions.PermissionDefault;

import dev.bcfcant.juicifruit.antessentials.AntEssentials;
import dev.bcfcant.juicifruit.antessentials.core.EssCommand;
import dev.bcfcant.juicifruit.antlib.core.util.MsgUtil;

public class HeadCommand extends EssCommand {

	public HeadCommand(AntEssentials ess) {
		super(ess, "head", "Gives the user the head of specified player", "ess.head", PermissionDefault.OP);
		this.setAliases(Arrays.asList("skull", "playerhead", "playerskull"));
		this.setUsage("/head [player]");
	}

	@Override
	public boolean run(CommandSender sender, String label, String[] args) {
		if (!isPlayer(sender)) {
			return true;
		}

		Player player = (Player) sender;
		String owner = player.getName();

		if (args.length > 0) {
			if (!args[0].matches("^[a-zA-Z0-9_]{2,16}$")) {
				MsgUtil.msg(player, "Head", ChatColor.RED + "Invalid player name.");
				return true;
			}
			owner = args[0];
		}

		if (Bukkit.getPlayerExact(owner) != null)
			owner = Bukkit.getPlayerExact(owner).getName();

		ItemStack skull = player.getItemInHand();
		SkullMeta meta = null;
		boolean spawn = false;

		if ((skull == null) || !(skull.getType().equals(Material.SKULL_ITEM)) || (skull.getDurability() != (short) 3)) {
			skull = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
			spawn = true;
		}
		meta = (SkullMeta) skull.getItemMeta();

		meta.setDisplayName(ChatColor.WHITE + owner + "'s Head");
		meta.setOwner(owner);

		skull.setItemMeta(meta);

		if (spawn) {
			HashMap<Integer, ItemStack> spare = player.getInventory().addItem(skull);
			if (!spare.isEmpty()) {
				MsgUtil.msg(player, "Head", ChatColor.RED + "Could not add head to your inventory, is it full?");
				return true;
			}
			MsgUtil.msg(player, "Head", ChatColor.GOLD + "Given you " + ChatColor.RED + owner + ChatColor.GOLD + "'s head.");
		} else {
			MsgUtil.msg(player, "Head", ChatColor.GOLD + "Set head to " + ChatColor.RED + owner + ".");
		}

		return true;
	}
}
