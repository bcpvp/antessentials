package dev.bcfcant.juicifruit.antessentials.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionDefault;

import dev.bcfcant.juicifruit.antessentials.AntEssentials;
import dev.bcfcant.juicifruit.antessentials.core.EssCommand;
import dev.bcfcant.juicifruit.antlib.core.util.MsgUtil;

public class SetWarpCommand extends EssCommand {

	public SetWarpCommand(AntEssentials ess) {
		super(ess, "setwarp", "Sets the warp to the player's location", "ess.setwarp", PermissionDefault.OP);
		this.setUsage("/setwarp <name>");
	}

	@Override
	public boolean run(CommandSender sender, String label, String[] args) {
		if (!isPlayer(sender)) {
			return true;
		}

		Player player = (Player) sender;

		if (args.length == 0) {
			sendUsageMessage(player);
			return true;
		}

		ess.getWarps().delWarp(args[0]);
		ess.getWarps().setWarp(args[0], player.getLocation());

		MsgUtil.msg(player, "Warp", ChatColor.GOLD + "Set warp " + ChatColor.RED + args[0]);

		return true;
	}
}
