package dev.bcfcant.juicifruit.antessentials.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.permissions.PermissionDefault;

import dev.bcfcant.juicifruit.antessentials.AntEssentials;
import dev.bcfcant.juicifruit.antessentials.core.EssCommand;
import dev.bcfcant.juicifruit.antlib.core.util.MsgUtil;

public class TpCommand extends EssCommand {

	public TpCommand(AntEssentials ess) {
		super(ess, "tp", "Changes the player's gamemode", "ess.tp", PermissionDefault.OP);
		this.setUsage("/tp (<player> [player])/(<x> <y> <z> [<pitch> <yaw>] [player])");
	}

	@Override
	public boolean run(CommandSender sender, String label, String[] args) {
		Player player = null;
		Player target = null;

		if (args.length == 1) {
			if (!(sender instanceof Player)) {
				sender.sendMessage(ChatColor.RED + "Usage: /tp <player> <player>");
				return true;
			}

			player = (Player) sender;
			target = Bukkit.getPlayer(args[0]);

			if (target == null) {
				MsgUtil.msg(sender, "TP", ChatColor.RED + "Could not find player: " + args[0]);
				return true;
			}

			player.teleport(target, TeleportCause.COMMAND);
			MsgUtil.msg(sender, "TP", ChatColor.GOLD + "Teleported to " + ChatColor.RED + target.getName());
		} else if (args.length == 2) {
			player = Bukkit.getPlayer(args[0]);
			target = Bukkit.getPlayer(args[1]);

			if (player == null) {
				MsgUtil.msg(sender, "TP", ChatColor.RED + "Could not find player: " + args[0]);
				return true;
			}
			if (target == null) {
				MsgUtil.msg(sender, "TP", ChatColor.RED + "Could not find player: " + args[1]);
				return true;
			}

			player.teleport(target, TeleportCause.COMMAND);
			MsgUtil.msg(sender, "TP", ChatColor.GOLD + "Teleported " + ChatColor.RED + player.getName() + ChatColor.GOLD + " to " + ChatColor.RED + target.getName());
		} else if (args.length >= 3) {
			try {
				if (sender instanceof Player) {
					player = (Player) sender;
				}

				double x = Double.parseDouble(args[0]);
				double y = Double.parseDouble(args[1]);
				double z = Double.parseDouble(args[2]);
				float pitch = 0F;
				float yaw = 0F;

				if (args.length >= 5) {
					pitch = Float.parseFloat(args[3]);
					yaw = Float.parseFloat(args[4]);

					if (args.length >= 6) {
						player = Bukkit.getPlayer(args[5]);
					}
				}

				if (player == null) {
					MsgUtil.msg(sender, "TP", ChatColor.RED + "You must be a player in game or specifiy a player to teleport.");
					return true;
				}

				player.teleport(new Location(player.getWorld(), x, y, z, yaw, pitch), TeleportCause.COMMAND);
				MsgUtil.msg(sender, "TP", ChatColor.GOLD + "Teleported " + ChatColor.RED + player.getName() + ChatColor.GOLD + " to " + ChatColor.RED + x + " " + y + " " + z);
			} catch (NumberFormatException e) {
				MsgUtil.msg(sender, "TP", ChatColor.RED + "Invalid coordinates.");
				return true;
			}
		}

		return true;
	}
}
