package dev.bcfcant.juicifruit.antessentials.commands;

import java.util.UUID;

import org.bukkit.BanList;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionDefault;

import dev.bcfcant.juicifruit.antessentials.AntEssentials;
import dev.bcfcant.juicifruit.antessentials.core.EssCommand;
import dev.bcfcant.juicifruit.antessentials.core.Permissions;
import dev.bcfcant.juicifruit.antessentials.core.Reference;
import dev.bcfcant.juicifruit.antessentials.core.player.PlayerLoader;
import dev.bcfcant.juicifruit.antessentials.core.player.User;
import dev.bcfcant.juicifruit.antlib.core.util.MsgUtil;

public class BanCommand extends EssCommand {

	public BanCommand(AntEssentials ess) {
		super(ess, "ban", "Ban the specified player", "ess.ban", PermissionDefault.OP);
		this.setUsage("/ban <player> [reason]");
	}

	@Override
	public boolean run(CommandSender sender, String label, String[] args) {
		if (args.length == 0) {
			sendUsageMessage(sender);
			return true;
		}

		UUID uuid = PlayerLoader.matchUser(args[0]);
		if (uuid == null) {
			MsgUtil.msg(sender, "Ban", ChatColor.RED + "Could not find player: " + args[0]);
			return true;
		}

		User user = ess.getUser(PlayerLoader.loadPlayer(uuid));

		if (user.hasPermission(Permissions.BAN_EXTEMPT)) {
			MsgUtil.msg(sender, "Ban", ChatColor.RED + user.getName() + ChatColor.GOLD + " is exempt from bans.");
			return true;
		}

		String banReason;
		if (args.length == 1) {
			banReason = Reference.DEFAULT_BAN_REASON;
		} else {
			banReason = ChatColor.translateAlternateColorCodes('&', getFinalArg(args, 1).replace("\\n", "\n").replace("|", "\n"));
		}

		String senderName = sender instanceof Player ? sender.getName() : "Console";

		ess.getServer().getBanList(BanList.Type.NAME).addBan(user.getName(), banReason, null, senderName);
		user.getBase().kickPlayer(ChatColor.DARK_RED + "Banned\n\n" + ChatColor.RED + banReason);

		String banDisplay = ChatColor.RED + senderName + ChatColor.GOLD + " banned " + ChatColor.RED + user.getName() + ChatColor.GOLD + " for " + ChatColor.RED + banReason
				+ ChatColor.GOLD + ".";

		ess.notify(Permissions.BAN_NOTIFY, "Ban", banDisplay);
		return true;
	}

	private static String getFinalArg(String[] args, int start) {
		StringBuilder bldr = new StringBuilder();
		for (int i = start; i < args.length; i++) {
			if (i != start) {
				bldr.append(" ");
			}
			bldr.append(args[i]);
		}
		return bldr.toString();
	}
}
