package dev.bcfcant.juicifruit.antessentials.commands;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.bukkit.BanList;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionDefault;

import com.google.common.base.Joiner;

import dev.bcfcant.juicifruit.antessentials.AntEssentials;
import dev.bcfcant.juicifruit.antessentials.core.EssCommand;
import dev.bcfcant.juicifruit.antessentials.core.Permissions;
import dev.bcfcant.juicifruit.antessentials.core.player.PlayerLoader;
import dev.bcfcant.juicifruit.antessentials.core.player.User;
import dev.bcfcant.juicifruit.antessentials.core.util.DateUtil;
import dev.bcfcant.juicifruit.antlib.core.util.MsgUtil;

public class TempbanCommand extends EssCommand {

	public TempbanCommand(AntEssentials ess) {
		super(ess, "tempban", "Tempban the specified player allowing the user to set a timeout", "ess.tempban", PermissionDefault.OP);
		this.setUsage("/tempban <player> r:<reason> t:<time>");
	}

	@Override
	public boolean run(CommandSender sender, String label, String[] args) {
		if (args.length < 4) {
			sendUsageMessage(sender);
			return true;
		}

		UUID uuid = PlayerLoader.matchUser(args[0]);
		if (uuid == null) {
			MsgUtil.msg(sender, "Ban", ChatColor.RED + "Unknown player: " + ChatColor.RED + args[0]);
			return true;
		}

		User user = ess.getUser(PlayerLoader.loadPlayer(uuid));

		if (user.hasPermission(Permissions.TEMPBAN_EXTEMPT)) {
			MsgUtil.msg(sender, "Ban", ChatColor.RED + user.getName() + ChatColor.GOLD + " is exempt from tempbans.");
			return true;
		}

		String string = Joiner.on(' ').join(args);
		if (!(string.toLowerCase().contains(" r:") && string.toLowerCase().contains(" t:"))) {
			MsgUtil.msg(sender, "Ban", ChatColor.RED + "You must specify a reason and time.");
			return true;
		}

		String banReason = ChatColor.translateAlternateColorCodes('&', StringUtils.substringBetween(string, " r:", " t:"));
		String time = StringUtils.substringAfterLast(string, " t:");

		long banTimestamp = -1L;
		try {
			banTimestamp = DateUtil.parseDateDiff(time, true);
		} catch (Exception e) {
		}

		if (banTimestamp == -1L) {
			MsgUtil.msg(sender, "Ban", ChatColor.RED + "Invalid ban length.");
			return true;
		}

		long maxBanLength = ess.getConfiguration().maxTempban * 1000L;
		if ((maxBanLength > 0L) && (banTimestamp - GregorianCalendar.getInstance().getTimeInMillis() > maxBanLength) && (sender instanceof Player)
				&& !sender.hasPermission(Permissions.TEMPBAN_UNLIMITED)) {
			MsgUtil.msg(sender, "Ban", "Cannot tempban for that long.");
			return true;
		}

		String senderName = sender instanceof Player ? sender.getName() : "Console";
		ess.getServer().getBanList(BanList.Type.NAME).addBan(user.getName(), banReason, new Date(banTimestamp), senderName);
		String expiry = DateUtil.formatDateDiff(banTimestamp);

		user.getBase().kickPlayer(ChatColor.DARK_RED + "Temporarily Banned\n\n" + ChatColor.RED + "Expiry: " + expiry + "\n\n" + ChatColor.RED + "Reason: " + banReason);

		String banNotify = ChatColor.RED + senderName + ChatColor.GOLD + " temporarily banned " + ChatColor.RED + user.getName() + ChatColor.GOLD + " for " + ChatColor.RED
				+ banReason + ChatColor.GOLD + ": " + ChatColor.RED + DateUtil.formatDateDiff(banTimestamp) + ChatColor.GOLD + ".";

		ess.notify(Permissions.TEMPBAN_NOTIFY, "Ban", banNotify);
		return true;
	}
}
