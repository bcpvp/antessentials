package dev.bcfcant.juicifruit.antessentials.api;

import java.util.Set;

import org.bukkit.BanEntry;
import org.bukkit.BanList;
import org.bukkit.OfflinePlayer;

import dev.bcfcant.juicifruit.antessentials.AntEssentials;
import dev.bcfcant.juicifruit.antessentials.core.player.User;

public class BanLookup {

	public static Boolean isBanned(AntEssentials ess, User user) {
		return isBanned(ess, user.getName());
	}

	public static Boolean isBanned(AntEssentials ess, OfflinePlayer player) {
		return player.isBanned();
	}

	public static Boolean isBanned(AntEssentials ess, String name) {
		return getBanEntry(ess, name) != null;
	}

	public static BanEntry getBanEntry(AntEssentials ess, User user) {
		return getBanEntry(ess, user.getName());
	}

	public static BanEntry getBanEntry(AntEssentials ess, String name) {
		Set<BanEntry> benteries = ess.getServer().getBanList(BanList.Type.NAME).getBanEntries();
		for (BanEntry banEnt : benteries) {
			if (banEnt.getTarget().equals(name)) {
				return banEnt;
			}
		}
		return null;
	}
}
