package dev.bcfcant.juicifruit.antessentials.api;

import org.bukkit.Location;

public class Warp {

	private String name;
	private Location loc;

	public Warp(String name, Location loc) {
		this.name = name;
		this.loc = loc;
	}

	public String getName() {
		return name;
	}

	public Location getLocation() {
		return loc;
	}
}
