package dev.bcfcant.juicifruit.antessentials.achievement;

import dev.bcfcant.juicifruit.antlib.api.achievement.GameAchievement;

public class ParkourAchievement extends GameAchievement {
    public ParkourAchievement() {
        super("parkourfinish", "Parkour Master", "Finish the parkour course");
    }
}
