package dev.bcfcant.juicifruit.antessentials;

import java.io.File;
import java.io.IOException;

import dev.bcfcant.juicifruit.antessentials.commands.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import dev.bcfcant.juicifruit.antessentials.core.ConfigManager;
import dev.bcfcant.juicifruit.antessentials.core.WarpManager;
import dev.bcfcant.juicifruit.antessentials.core.analytics.AnalyticOnlinePlayers;
import dev.bcfcant.juicifruit.antessentials.core.analytics.AnalyticUniquePlayers;
import dev.bcfcant.juicifruit.antessentials.core.analytics.AnalyticVanishedPlayers;
import dev.bcfcant.juicifruit.antessentials.core.listeners.PlayerListener;
import dev.bcfcant.juicifruit.antessentials.core.player.User;
import dev.bcfcant.juicifruit.antessentials.core.player.UserMap;
import dev.bcfcant.juicifruit.antessentials.core.report.ReportManager;
import dev.bcfcant.juicifruit.antessentials.core.util.LogUtil;
import dev.bcfcant.juicifruit.antlib.api.analytics.AnalyticManager;
import dev.bcfcant.juicifruit.antlib.core.CommandManager;
import dev.bcfcant.juicifruit.antlib.core.util.MsgUtil;
import dev.bcfcant.juicifruit.antlib.core.util.yaml.YAMLFormat;
import dev.bcfcant.juicifruit.antlib.core.util.yaml.YAMLProcessor;

public class AntEssentials extends JavaPlugin {

	private ConfigManager config;
	private UserMap userMap;
	private WarpManager warpManager;
	private ReportManager reportManager;

	private static AntEssentials instance;

	@Override
	public void onEnable() {
		instance = this;

		LogUtil.init(this);
		initConfig();

		userMap = new UserMap(this);
		warpManager = new WarpManager(this);
		reportManager = new ReportManager(this);

		registerCommands();
		registerAnalyticElements();
		registerListeners(Bukkit.getPluginManager());
	}

	@Override
	public void onDisable() {
		instance = null;
	}

	private void registerAnalyticElements() {
		AnalyticManager.getInstance().registerElement(new AnalyticUniquePlayers());
		AnalyticManager.getInstance().registerElement(new AnalyticOnlinePlayers());
		AnalyticManager.getInstance().registerElement(new AnalyticVanishedPlayers());
	}

	private void initConfig() {
		getDataFolder().mkdirs();
		File configFile = new File(getDataFolder(), "config.yml");
		if (!configFile.exists()) {
			try {
				configFile.createNewFile();
			} catch (IOException e) {
				getLogger().severe("Could not create configuration file: " + e.getMessage());
			}
		}

		config = new ConfigManager(this, new YAMLProcessor(configFile, true, YAMLFormat.EXTENDED));
		config.load();
	}

	private void registerListeners(PluginManager pm) {
		pm.registerEvents(new PlayerListener(this), this);
	}

	private void registerCommands() {
		CommandManager.register(this, new MuteCommand(this));
		CommandManager.register(this, new KickCommand(this));
		CommandManager.register(this, new TempbanCommand(this));
		CommandManager.register(this, new BanCommand(this));
		CommandManager.register(this, new BanIPCommand(this));
		CommandManager.register(this, new UnbanCommand(this));
		CommandManager.register(this, new UnbanIPCommand(this));
        CommandManager.register(this, new IsBannedCommand(this));
        CommandManager.register(this, new WhoisCommand(this));

		CommandManager.register(this, new VanishCommand(this));
		CommandManager.register(this, new HeadCommand(this));
		CommandManager.register(this, new FlyCommand(this));
		CommandManager.register(this, new GameModeCommand(this));
		CommandManager.register(this, new TpCommand(this));
		CommandManager.register(this, new GiveCommand(this));
		CommandManager.register(this, new ItemCommand(this));

		CommandManager.register(this, new SetWarpCommand(this));
		CommandManager.register(this, new DelWarpCommand(this));

		CommandManager.register(this, new ListCommand(this));
		CommandManager.register(this, new WarpCommand(this));
		CommandManager.register(this, new PingCommand(this));
		CommandManager.register(this, new GhostFixCommand(this));
		CommandManager.register(this, new SpawnCommand(this));

		CommandManager.register(this, new AnalyseCommand(this));
	}

	public ConfigManager getConfiguration() {
		return config;
	}

	public UserMap getUserMap() {
		return userMap;
	}

	public WarpManager getWarps() {
		return warpManager;
	}

	public ReportManager getReportManager() {
		return reportManager;
	}

	public User getUser(Player player) {
		if (player == null)
			return null;

		if (userMap == null) {
			LogUtil.warning("AntEssentials userMap not initialised");
			return null;
		}

		User user = userMap.getUser(player.getUniqueId());

		if (user == null) {
			user = new User(player, this);
		} else {
			user.update(player);
		}
		return user;
	}

	public void notify(Permission perm, String msg) {
		for (Player player : this.getServer().getOnlinePlayers()) {
			if (player.hasPermission(perm))
				MsgUtil.msg(player, msg);
		}
		LogUtil.info(ChatColor.stripColor(msg));
	}

	public void notify(Permission perm, String type, String msg) {
		for (Player player : this.getServer().getOnlinePlayers()) {
			if (player.hasPermission(perm))
				MsgUtil.msg(player, type, msg);
		}
		LogUtil.info(ChatColor.stripColor("[" + type + "] " + msg));
	}

	public String matchPlayer(String name) {
		String match = null;
		boolean matched = false;

		if (getServer().getPlayerExact(name) != null) {
			return getServer().getPlayerExact(name).getName();
		}

		if (getServer().getPlayer(name) != null) {
			return getServer().getPlayer(name).getName();
		}

		if (!matched) {
			for (OfflinePlayer op : getServer().getOfflinePlayers()) {
				if (op.getName().equalsIgnoreCase(name)) {
					match = op.getName();
					matched = true;
					break;
				}
			}

			if (!matched) {
				for (OfflinePlayer op : getServer().getOfflinePlayers()) {
					if (op.getName().toLowerCase().startsWith(name.toLowerCase())) {
						match = op.getName();
						matched = true;
						break;
					}
				}
				/*
				 * if (!matched) { for (OfflinePlayer op : getServer().getOfflinePlayers()) { if
				 * (op.getName().toLowerCase().contains(name.toLowerCase())) { match = op.getName(); matched = true;
				 * break; } } }
				 */
			}
		}

		return match;
	}

	public static AntEssentials getInstance() {
		return instance;
	}
}
