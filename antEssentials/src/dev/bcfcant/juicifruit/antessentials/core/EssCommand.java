package dev.bcfcant.juicifruit.antessentials.core;

import org.bukkit.permissions.PermissionDefault;

import dev.bcfcant.juicifruit.antessentials.AntEssentials;
import dev.bcfcant.juicifruit.antlib.api.AbstractCommand;

public abstract class EssCommand extends AbstractCommand {

	protected AntEssentials ess;

	public EssCommand(AntEssentials ess, String name, String description, String permission, PermissionDefault permissionDefault) {
		super(ess, name, description, permission, permissionDefault);
		this.ess = ess;
	}
}
