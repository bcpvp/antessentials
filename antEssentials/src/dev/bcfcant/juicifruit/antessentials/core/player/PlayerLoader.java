package dev.bcfcant.juicifruit.antessentials.core.player;

import java.io.File;
import java.util.UUID;

import net.minecraft.server.v1_7_R4.EntityPlayer;
import net.minecraft.server.v1_7_R4.MinecraftServer;
import net.minecraft.server.v1_7_R4.PlayerInteractManager;
import net.minecraft.server.v1_7_R4.WorldServer;
import net.minecraft.util.com.mojang.authlib.GameProfile;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_7_R4.CraftServer;
import org.bukkit.entity.Player;

public class PlayerLoader {

	public static Player loadPlayer(UUID uuid) {
		if (Bukkit.getServer().getPlayer(uuid) != null)
			return Bukkit.getServer().getPlayer(uuid);

		File playerFolder = new File(((World) Bukkit.getWorlds().get(0)).getWorldFolder(), "playerdata");
		if (!playerFolder.exists()) {
			return null;
		}

		boolean noPlayer = true;
		for (OfflinePlayer player : Bukkit.getServer().getOfflinePlayers()) {
			if (player.getUniqueId().equals(uuid)) {
				noPlayer = false;
				break;
			}
		}

		if (noPlayer)
			return null;

		OfflinePlayer player = Bukkit.getOfflinePlayer(uuid);

		GameProfile gameprofile = new GameProfile(player.getUniqueId(), player.getName());

		MinecraftServer minecraftserver = ((CraftServer) Bukkit.getServer()).getServer();
		WorldServer worldserver = minecraftserver.getWorldServer(0);
		PlayerInteractManager playerinteractmanager = new PlayerInteractManager(worldserver);

		EntityPlayer entityPlayer = new EntityPlayer(minecraftserver, worldserver, gameprofile, playerinteractmanager);

		Player target = entityPlayer.getBukkitEntity();

		if (target != null) {
			target.loadData();
			return target;
		}

		return null;
	}

	public static Player loadPlayer(String name) {
		if (Bukkit.getServer().getPlayer(name) != null)
			return Bukkit.getServer().getPlayer(name);

		UUID uuid = matchUser(name);
		if (uuid == null)
			return null;

		Player target = loadPlayer(uuid);
		if (target != null) {
			return target;
		}

		return null;
	}

	public static UUID matchUser(String search) {
		File playerFolder = new File(((World) Bukkit.getWorlds().get(0)).getWorldFolder(), "playerdata");
		if (!playerFolder.exists() || !playerFolder.isDirectory())
			return null;
		if (search == null)
			return null;

		UUID found = null;

		String lowerSearch = search.toLowerCase();
		int delta = 2147483647;

		for (File f : playerFolder.listFiles()) {
			if (!f.getName().endsWith(".dat"))
				continue;
			String uuidString = f.getName().substring(0, f.getName().length() - 4);
			UUID uuid = UUID.fromString(uuidString);

			OfflinePlayer player = Bukkit.getOfflinePlayer(uuid);
			String name = player.getName();

			if (name.equalsIgnoreCase(search))
				return uuid;

			if (name.toLowerCase().startsWith(lowerSearch)) {
				int curDelta = name.length() - lowerSearch.length();
				if (curDelta < delta) {
					found = player.getUniqueId();
					delta = curDelta;
				}
				if (curDelta == 0) {
					break;
				}
			}
		}

		return found;
	}
}
