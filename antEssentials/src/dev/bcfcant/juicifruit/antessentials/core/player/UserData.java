package dev.bcfcant.juicifruit.antessentials.core.player;

import java.io.File;

import org.bukkit.Location;
import org.bukkit.Server;
import org.bukkit.entity.Player;

import dev.bcfcant.juicifruit.antessentials.AntEssentials;

public class UserData {

	protected transient final AntEssentials ess;
	private final UserConf config;
	private final File folder;

	protected Player base;

	private boolean muted;
	private long muteTimeout;
	private boolean socialSpy;
	private String lastLoginAddress;

	protected UserData(Player base, AntEssentials ess) {
		this.ess = ess;
		this.base = base;
		this.folder = new File(ess.getDataFolder(), "userdata");
		if (!this.folder.exists())
			this.folder.mkdirs();
		this.config = new UserConf(new File(this.folder, this.base.getUniqueId().toString() + ".yml"));
		this.reloadConfig();
	}

	public Player getBase() {
		return this.base;
	}

	public void setBase(Player base) {
		this.base = base;
	}

	public Server getServer() {
		return base.getServer();
	}

	public Location getLocation() {
		return base.getLocation();
	}

	public final void reset() {
		this.config.getFile().delete();
	}

	public final void reloadConfig() {
		this.config.load();
		this.muted = _isMuted();
		this.muteTimeout = _getMuteTimeout();
		this.socialSpy = _isSocialSpyEnabled();
		this.lastLoginAddress = _getLastLoginAddress();
	}

	private boolean _isMuted() {
		return this.config.getBoolean("mute.muted", false);
	}

	public boolean isMuted() {
		return this.muted;
	}

	public void setMuted(boolean muted) {
		this.muted = muted;
		this.config.set("mute.muted", muted);
		this.config.save();
	}

	private long _getMuteTimeout() {
		return this.config.getLong("mute.timeout", 0L);
	}

	public long getMuteTimeout() {
		return this.muteTimeout;
	}

	public void setMuteTimeout(long muteTimeout) {
		this.muteTimeout = muteTimeout;
		this.config.set("mute.timeout", muteTimeout);
		this.config.save();
	}

	private boolean _isSocialSpyEnabled() {
		return this.config.getBoolean("social-spy", false);
	}

	public boolean isSocialSpyEnabled() {
		return this.socialSpy;
	}

	public void setSocialSpyEnabled(boolean enabled) {
		this.socialSpy = enabled;
		this.config.set("social-spy", socialSpy);
		this.config.save();
	}

	private String _getLastLoginAddress() {
		return config.getString("ipAddress", "");
	}

	public String getLastLoginAddress() {
		return lastLoginAddress;
	}

	private void _setLastLoginAddress(String address) {
		lastLoginAddress = address;
		config.set("ipAddress", address);
	}

	public void updateAddress() {
		if (base.getAddress() != null && base.getAddress().getAddress() != null) {
			_setLastLoginAddress(base.getAddress().getAddress().getHostAddress());
		}
		this.config.save();
	}

	public void trackName() {
		this.config.set("name", base.getName());
		this.config.save();
	}

	public void save() {
		this.config.save();
	}
}
