package dev.bcfcant.juicifruit.antessentials.core.player;

import java.io.File;
import java.util.Collections;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.ExecutionException;

import org.bukkit.entity.Player;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.util.concurrent.UncheckedExecutionException;

import dev.bcfcant.juicifruit.antessentials.AntEssentials;

public class UserMap extends CacheLoader<UUID, User> {

	private final transient AntEssentials ess;
	private final transient Cache<UUID, User> users;
	private final transient ConcurrentSkipListSet<UUID> keys = new ConcurrentSkipListSet<UUID>();

	public UserMap(AntEssentials ess) {
		this.ess = ess;
		this.users = CacheBuilder.newBuilder().maximumSize(ess.getConfiguration().maxUserCacheCount).softValues().build(this);
		loadAllUsersAsync(ess);
	}

	private void loadAllUsersAsync(final AntEssentials ess) {
		ess.getServer().getScheduler().runTaskAsynchronously(ess, new Runnable() {
			public void run() {
				File userdir = new File(ess.getDataFolder(), "userdata");
				if (!userdir.exists()) {
					return;
				}
				UserMap.this.keys.clear();
				UserMap.this.users.invalidateAll();
				for (String string : userdir.list()) {
					if (string.endsWith(".yml")) {
						String uuidString = string.substring(0, string.length() - 4);
						UUID uuid = UUID.fromString(uuidString);
						UserMap.this.keys.add(uuid);
					}
				}
			}
		});
	}

	public boolean userExists(UUID uuid) {
		return this.keys.contains(uuid);
	}

	public User getUser(UUID uuid) {
		try {
			return (User) this.users.get(uuid);
		} catch (ExecutionException ex) {
			return null;
		} catch (UncheckedExecutionException ex) {
		}
		return null;
	}

	public User load(UUID uuid) throws Exception {
		for (Player player : this.ess.getServer().getOnlinePlayers()) {
			if (player.getUniqueId().equals(uuid)) {
				this.keys.add(uuid);
				return new User(player, this.ess);
			}
		}
		File userFile = getUserFile2(uuid);
		if (userFile.exists()) {
			this.keys.add(uuid);
			return new User(PlayerLoader.loadPlayer(uuid), this.ess);
		}
		throw new Exception("User not found!");
	}

	public void reloadConfig() {
		loadAllUsersAsync(this.ess);
	}

	public void removeUser(UUID uuid) {
		this.keys.remove(uuid);
		this.users.invalidate(uuid);
	}

	public Set<UUID> getAllUniqueUsers() {
		return Collections.unmodifiableSet(this.keys);
	}

	public int getUniqueUsers() {
		return this.keys.size();
	}

	public File getUserFile(UUID uuid) {
		return getUserFile2(uuid);
	}

	private File getUserFile2(UUID uuid) {
		File userFolder = new File(this.ess.getDataFolder(), "userdata");
		return new File(userFolder, uuid.toString() + ".yml");
	}
}