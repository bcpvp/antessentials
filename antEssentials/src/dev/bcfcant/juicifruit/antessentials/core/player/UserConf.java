package dev.bcfcant.juicifruit.antessentials.core.player;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CoderResult;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;

import dev.bcfcant.juicifruit.antessentials.core.util.LogUtil;

public class UserConf extends YamlConfiguration {

	private static final Charset UTF8 = Charset.forName("UTF-8");

	private File configFile;

	private final byte[] bytebuffer = new byte[1024];

	public UserConf(File configFile) {
		this.configFile = configFile.getAbsoluteFile();
	}

	public void load() {
		if (!this.configFile.getParentFile().exists())
			this.configFile.getParentFile().mkdirs();
		if (!this.configFile.exists()) {
			try {
				this.configFile.createNewFile();
			} catch (IOException e) {
				LogUtil.severe("Error creating player config: " + configFile.getName());
			}
		}

		try {
			FileInputStream in = new FileInputStream(this.configFile);

			try {
				long startSize = this.configFile.length();
				if (startSize > Integer.MAX_VALUE) {
					throw new InvalidConfigurationException("File too large");
				}

				ByteBuffer buffer = ByteBuffer.allocate((int) startSize);

				int l;
				while ((l = in.read(this.bytebuffer)) != -1) {
					if (l > buffer.remaining()) {
						ByteBuffer resize = ByteBuffer.allocate(buffer.capacity() + l - buffer.remaining());
						int resizePosition = buffer.position();
						buffer.rewind();
						resize.put(buffer);
						resize.position(resizePosition);
						buffer = resize;
					}
					buffer.put(this.bytebuffer, 0, l);
				}
				buffer.rewind();

				CharBuffer data = CharBuffer.allocate(buffer.capacity());
				CharsetDecoder decoder = UTF8.newDecoder();
				CoderResult result = decoder.decode(buffer, data, true);

				if (result.isError()) {
					buffer.rewind();
					data.clear();
					decoder = Charset.defaultCharset().newDecoder();
					result = decoder.decode(buffer, data, true);
					if (result.isError()) {
						throw new InvalidConfigurationException(new StringBuilder().append("Invalid Characters in file ").append(this.configFile.getAbsolutePath().toString()).toString());
					}

					decoder.flush(data);
				} else {
					decoder.flush(data);
				}
				int end = data.position();
				data.rewind();
				super.loadFromString(data.subSequence(0, end).toString());
			} finally {
				in.close();
			}
		} catch (IOException e) {
			LogUtil.severe("Error loading player config: " + configFile.getName());
		} catch (InvalidConfigurationException e) {
			File broken = new File(new StringBuilder().append(this.configFile.getAbsolutePath()).append(".broken.").append(System.currentTimeMillis()).toString());
			this.configFile.renameTo(broken);
		}
	}

	public File getFile() {
		return this.configFile;
	}

	public void save() {
		try {
			save(this.configFile);
		} catch (IOException ex) {
			LogUtil.severe("Error saving player config: " + this.configFile.getName());
		}
	}
}
