package dev.bcfcant.juicifruit.antessentials.core.player;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;

import dev.bcfcant.juicifruit.antessentials.AntEssentials;
import dev.bcfcant.juicifruit.antlib.api.VanishAPI;
import dev.bcfcant.juicifruit.antlib.core.util.MsgUtil;

public class User extends UserData implements Comparable<User> {

	public User(Player base, AntEssentials ess) {
		super(base, ess);
		this.base = base;
	}

	public User update(Player base) {
		setBase(base);
		return this;
	}

	public void sendMessage(String message) {
		getBase().sendMessage(message);
	}

	public void sendUtilMessage(String message) {
		MsgUtil.msg(getBase(), message);
	}

	public void sendUtilMessage(String type, String message) {
		MsgUtil.msg(getBase(), type, message);
	}

	public boolean hasPermission(Permission perm) {
		return getBase().hasPermission(perm);
	}

	public boolean hasPermission(String name) {
		return getBase().hasPermission(name);
	}

	public String getName() {
		return getBase().getName();
	}

	public String getDisplayName() {
		return getBase().getDisplayName();
	}

	public boolean checkMuteTimeout(long currentTime) {
		if ((getMuteTimeout() > 0L) && (getMuteTimeout() < currentTime) && (isMuted())) {
			setMuteTimeout(0L);
			sendUtilMessage("Chat", ChatColor.GOLD + "You are now able to talk again.");
			setMuted(false);
			return true;
		}
		return false;
	}

	public void setVanished(boolean vanish) {
		VanishAPI.setVanished(getBase(), vanish);
	}

	public void toggleVanish() {
		VanishAPI.toggleVanish(getBase());
	}

	public void vanish() {
		VanishAPI.vanish(getBase());
	}

	public void unvanish() {
		VanishAPI.unvanish(getBase());
	}

	@Override
	public int compareTo(User other) {
		return getBase().getDisplayName().compareToIgnoreCase(other.getBase().getDisplayName());
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof User)) {
			return false;
		}
		return getName().equalsIgnoreCase(((User) object).getName());
	}

	@Override
	public int hashCode() {
		return getBase().hashCode();
	}
}
