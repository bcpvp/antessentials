package dev.bcfcant.juicifruit.antessentials.core;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Nonnull;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import dev.bcfcant.juicifruit.antessentials.AntEssentials;
import dev.bcfcant.juicifruit.antessentials.api.Warp;
import dev.bcfcant.juicifruit.antessentials.core.util.LogUtil;

public class WarpManager {

	private AntEssentials ess;

	private File file;
	private FileConfiguration config;

	private Map<String, Location> warps = new HashMap<String, Location>();

	public WarpManager(AntEssentials ess) {
		this.ess = ess;

		file = new File(ess.getDataFolder(), "warps.yml");
		if (!file.getParentFile().exists())
			file.getParentFile().mkdirs();
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				LogUtil.severe("Could not load warps file");
			}
		}

		config = YamlConfiguration.loadConfiguration(file);

		load();
	}

	public void load() {
		config = YamlConfiguration.loadConfiguration(file);

		for (String key : config.getKeys(false)) {
			ConfigurationSection cs = config.getConfigurationSection(key);
			if (cs != null) {
				try {
					String worldName = cs.getString("world", "world");
					World world = ess.getServer().getWorld(worldName);
					double x = cs.getDouble("x");
					double y = cs.getDouble("y");
					double z = cs.getDouble("z");
					float yaw = (float) cs.getDouble("yaw");
					float pitch = (float) cs.getDouble("pitch");
					Location loc = new Location(world, x, y, z, yaw, pitch);

					warps.put(key, loc);
				} catch (Throwable ignored) {
				}
			}
		}
	}

	public void save() {
		config = new YamlConfiguration();
		for (Entry<String, Location> warp : warps.entrySet()) {
			ConfigurationSection cs = config.createSection(warp.getKey());
			cs.set("world", warp.getValue().getWorld().getName());
			cs.set("x", warp.getValue().getX());
			cs.set("y", warp.getValue().getY());
			cs.set("z", warp.getValue().getZ());
			cs.set("yaw", (double) warp.getValue().getYaw());
			cs.set("pitch", (double) warp.getValue().getPitch());
		}
		try {
			config.save(file);
		} catch (IOException e) {
			LogUtil.severe("Could not save warps file");
		}
	}

	public Map<String, Location> getWarps() {
		return warps;
	}

	public Warp getWarp(@Nonnull String name) {
		for (Entry<String, Location> warp : warps.entrySet()) {
			if (warp.getKey().equalsIgnoreCase(name)) {
				return new Warp(warp.getKey(), warp.getValue());
			}
		}
		return null;
	}

	public void setWarp(@Nonnull String name, @Nonnull Location location) {
		delWarp(name);
		warps.put(name, location);
		save();
	}

	public boolean delWarp(@Nonnull String name) {
		boolean bool = false;
		Warp warp;
		if ((warp = getWarp(name)) != null) {
			bool = warps.remove(warp.getName()) != null;
		} else {
			bool = warps.remove(name) != null;
		}
		save();
		return bool;
	}
}
