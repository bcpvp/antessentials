package dev.bcfcant.juicifruit.antessentials.core.util;

import java.util.regex.Pattern;

public class FormatUtil {

	public static final Pattern IPPATTERN = Pattern.compile("^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
			+ "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])$");

	public static boolean validIP(String ipAddress) {
		return IPPATTERN.matcher(ipAddress).matches();
	}
}
