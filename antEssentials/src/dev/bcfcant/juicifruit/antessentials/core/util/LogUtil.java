package dev.bcfcant.juicifruit.antessentials.core.util;

import java.util.logging.Level;
import java.util.logging.Logger;

import dev.bcfcant.juicifruit.antessentials.AntEssentials;

public class LogUtil {

	private static Logger log = Logger.getLogger("AntEssentials");

	public static void init(AntEssentials plugin) {
		log = plugin.getLogger();
	}

	public static void log(Level level, String msg) {
		log.log(level, msg);
	}

	public static void finest(String msg) {
		log.finest(msg);
	}

	public static void finer(String msg) {
		log.finer(msg);
	}

	public static void fine(String msg) {
		log.fine(msg);
	}

	public static void info(String msg) {
		log.info(msg);
	}

	public static void warning(String msg) {
		log.warning(msg);
	}

	public static void severe(String msg) {
		log.severe(msg);
	}

	public static void config(String msg) {
		log.config(msg);
	}

	public static Logger getLogger() {
		return log;
	}
}
