package dev.bcfcant.juicifruit.antessentials.core;

import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;

public class Permissions {

	public static final Permission MUTE_EXTEMPT = new Permission("ess.mute.exempt", "Provides the player exemption from mutes.", PermissionDefault.OP);
	public static final Permission KICK_EXTEMPT = new Permission("ess.kick.exempt", "Provides the player exemption from kicks.", PermissionDefault.OP);
	public static final Permission TEMPBAN_EXTEMPT = new Permission("ess.tempban.exempt", "Provides the player exemption from tempbans.", PermissionDefault.OP);
	public static final Permission BAN_EXTEMPT = new Permission("ess.ban.exempt", "Provides the player exemption from bans.", PermissionDefault.OP);

	public static final Permission MUTE_NOTIFY = new Permission("ess.mute.notify", "Notifies the player of mutes.", PermissionDefault.OP);
	public static final Permission KICK_NOTIFY = new Permission("ess.kick.notify", "Notifies the player of kicks.", PermissionDefault.OP);
	public static final Permission TEMPBAN_NOTIFY = new Permission("ess.tempban.notify", "Notifies the player of tempbans.", PermissionDefault.OP);
	public static final Permission BAN_NOTIFY = new Permission("ess.ban.notify", "Notifies the player of bans.", PermissionDefault.OP);

	public static final Permission TEMPBAN_UNLIMITED = new Permission("ess.tempban.unlimited", "Allows the player to tempban players for an unlimited amount of time.", PermissionDefault.OP);
	public static final Permission WARP_OTHER = new Permission("ess.warp.other", "Allows the player to warp other players.", PermissionDefault.OP);
	public static final Permission JOIN_FULL_SERVER = new Permission("ess.joinfull", "Allows the player to join the server when full.", PermissionDefault.OP);
}
