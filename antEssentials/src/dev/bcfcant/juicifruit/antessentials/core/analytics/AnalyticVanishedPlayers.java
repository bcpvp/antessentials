package dev.bcfcant.juicifruit.antessentials.core.analytics;

import dev.bcfcant.juicifruit.antlib.api.VanishAPI;
import dev.bcfcant.juicifruit.antlib.api.analytics.AnalyticElement;

public class AnalyticVanishedPlayers extends AnalyticElement {
	public AnalyticVanishedPlayers() {
		super("vanishedPlayers", "Vanished Players");
	}

	@Override
	public Object getValue() {
		return VanishAPI.getVanishedPlayers().size();
	}
}
