package dev.bcfcant.juicifruit.antessentials.core.analytics;

import org.bukkit.Bukkit;

import dev.bcfcant.juicifruit.antlib.api.analytics.AnalyticElement;

public class AnalyticUniquePlayers extends AnalyticElement {
	public AnalyticUniquePlayers() {
		super("uniquePlayers", "Unique Players");
	}

	@Override
	public Object getValue() {
		return Bukkit.getOfflinePlayers().length;
	}
}
