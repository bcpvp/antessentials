package dev.bcfcant.juicifruit.antessentials.core.analytics;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import dev.bcfcant.juicifruit.antlib.api.analytics.AnalyticElement;

public class AnalyticOnlinePlayers extends AnalyticElement {
	public AnalyticOnlinePlayers() {
		super("onlinePlayers", "Online Players");
	}

	@Override
	public Object getValue() {
		return onlinePlayers();
	}

	private int onlinePlayers() {
		int i = 0;

		for (@SuppressWarnings("unused")
		Player player : Bukkit.getOnlinePlayers())
			i++;

		return i;
	}
}
