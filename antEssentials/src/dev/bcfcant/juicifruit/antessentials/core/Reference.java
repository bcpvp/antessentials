package dev.bcfcant.juicifruit.antessentials.core;

public class Reference {

	public static final String DEFAULT_BAN_REASON = "You have been banned!";
	public static final String DEFAULT_KICK_REASON = "You have been kicked!";
}
