package dev.bcfcant.juicifruit.antessentials.core.report;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import net.minecraft.util.org.apache.commons.io.FileUtils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;

import dev.bcfcant.juicifruit.antessentials.AntEssentials;
import dev.bcfcant.juicifruit.antessentials.core.util.LogUtil;

public class PlayerChatLog {

	private transient AntEssentials _ess;

	private UUID player;
	private List<String> log;
	private File logFile;

	public PlayerChatLog(UUID player) {
		this(AntEssentials.getInstance(), player);
	}

	public PlayerChatLog(AntEssentials ess, UUID player) {
		this.player = player;
		this.log = new ArrayList<String>();
		try {
			logFile = generateFile();
		} catch (IOException e) {
			LogUtil.severe("Unable to create log file for user " + getPlayer().getName());
		}

		this._ess = ess;
	}

	private final AntEssentials ess() {
		return _ess == null ? (_ess = AntEssentials.getInstance()) : _ess;
	}

	public void addToLog(String message) {
		String r = ChatColor.stripColor(message);
		Date date = new Date(System.currentTimeMillis());
		SimpleDateFormat ft = new SimpleDateFormat("[yyyy-MM-dd] [zzz HH:mm:ss]");
		r = ft.format(date) + " " + r;

		if (log.size() == 100)
			log.remove(0);

		log.add(r);

		try {
			writeFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static PlayerChatLog fromFile(AntEssentials ess, OfflinePlayer player) {
		return new PlayerChatLog(ess, player.getUniqueId());
	}

	public void loadFile() throws IOException {
		log.clear();

		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(logFile));

			String line;
			while ((line = br.readLine()) != null) {
				log.add(line);
			}
		} catch (IOException e) {
			throw e;
		} finally {
			if (br != null)
				br.close();
		}
	}

	public OfflinePlayer getPlayer() {
		return Bukkit.getOfflinePlayer(player);
	}

	private void writeFile() throws IOException {
		FileUtils.write(logFile, ""); // Clears the file
		FileUtils.writeLines(logFile, log); // Write in the file
	}

	public File generateFile() throws IOException {
		File file = new File(ess().getDataFolder(), "/chat/" + getPlayer().getName() + ".txt");

		if (!file.exists()) {
			file.createNewFile();
		} else {
			logFile = file;
			loadFile();
		}

		return file;
	}
}
