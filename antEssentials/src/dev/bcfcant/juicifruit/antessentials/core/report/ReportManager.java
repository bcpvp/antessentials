package dev.bcfcant.juicifruit.antessentials.core.report;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.OfflinePlayer;

import dev.bcfcant.juicifruit.antessentials.AntEssentials;

public class ReportManager {

	private AntEssentials ess;
	private List<PlayerChatLog> logs = new ArrayList<PlayerChatLog>();

	public ReportManager(AntEssentials ess) {
		this.ess = ess;
	}

	public List<PlayerChatLog> getLogs() {
		return logs;
	}

	private void loadLog(OfflinePlayer player) {
		logs.add(PlayerChatLog.fromFile(ess, player));
	}

	public PlayerChatLog getLog(OfflinePlayer player) {
		for (PlayerChatLog log : logs) {
			if (log.getPlayer().equals(player))
				return log;
		}

		loadLog(player);
		return getLog(player);
	}
}
