package dev.bcfcant.juicifruit.antessentials.core.listeners;

import dev.bcfcant.juicifruit.antessentials.achievement.ParkourAchievement;
import dev.bcfcant.juicifruit.antlib.api.achievement.GameAchievements;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.FishHook;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.*;
import org.bukkit.scheduler.BukkitRunnable;

import dev.bcfcant.juicifruit.antessentials.AntEssentials;
import dev.bcfcant.juicifruit.antessentials.core.Permissions;
import dev.bcfcant.juicifruit.antessentials.core.player.User;
import dev.bcfcant.juicifruit.antessentials.core.util.LogUtil;

public class PlayerListener extends EssListener {

	public PlayerListener(AntEssentials ess) {
		super(ess);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onChat(AsyncPlayerChatEvent event) {
		User user = ess.getUser(event.getPlayer());

		if (user.isMuted() && !user.hasPermission(Permissions.MUTE_EXTEMPT)) {
			event.setCancelled(true);
			user.sendUtilMessage("Chat", ChatColor.RED + "You are muted.");
			LogUtil.info("Muted player '" + user.getName() + "' attempted to chat");
		}

		ess.getReportManager().getLog(event.getPlayer()).addToLog(event.getPlayer().getName() + ": " + event.getMessage());

	}

	@EventHandler(priority = EventPriority.HIGH)
	public void onLogin(PlayerLoginEvent event) {
		User user = ess.getUser(event.getPlayer());

		switch (event.getResult()) {
		case KICK_FULL:
			if (user.hasPermission(Permissions.JOIN_FULL_SERVER)) {
				event.allow();
				return;
			}
			event.disallow(PlayerLoginEvent.Result.KICK_FULL, ChatColor.RED + "Server is full!");
			break;
		default:
			break;
		}
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		User user = ess.getUser(event.getPlayer());

		user.checkMuteTimeout(System.currentTimeMillis());
		user.trackName();

		ess.getReportManager().getLog(event.getPlayer()).addToLog(">> " + event.getPlayer().getName() + " logs in.");
	}

	@EventHandler
	public void onQuit(PlayerQuitEvent event) {
		ess.getReportManager().getLog(event.getPlayer()).addToLog("<< " + event.getPlayer().getName() + " logs off.");
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent event) {
		Player player = event.getPlayer();
		String cmd = event.getMessage().toLowerCase().split(" ")[0].replace("/", "").toLowerCase();
		if (this.ess.getConfiguration().socialSpyCommands.contains(cmd)) {
			for (Player onlinePlayer : this.ess.getServer().getOnlinePlayers()) {
				User spyer = this.ess.getUser(onlinePlayer);
				if ((spyer.isSocialSpyEnabled()) && (!player.equals(onlinePlayer))) {
					spyer.sendMessage(player.getName() + ": " + event.getMessage());
				}
			}
		}
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onFishingRodHit(EntityDamageByEntityEvent event) {
		if (!(event.getEntity() instanceof Player))
			return;
		final Player player = (Player) event.getEntity();

		if (player.getNoDamageTicks() > 0)
			return;

		if (!(event.getDamager() instanceof FishHook))
			return;

		new BukkitRunnable() {
			@Override
			public void run() {
				player.setNoDamageTicks(0);
			}
		}.runTask(ess);
	}

    @EventHandler
    public void onPlayerTriggerPressureplate(PlayerInteractEvent event) {
        final Player player = event.getPlayer();

        if (event.getAction() != Action.PHYSICAL)
            return;

        if (event.getClickedBlock() == null)
            return;

        if (event.getClickedBlock().getType() == Material.GOLD_PLATE) {
            Location plate = event.getClickedBlock().getLocation().clone();

            if (plate.subtract(0, 2, 0).getBlock().getType() == Material.GOLD_BLOCK && plate.subtract(0, 1, 0).getBlock().getType() == Material.GOLD_BLOCK) {
                player.teleport(Bukkit.getWorlds().get(0).getSpawnLocation());
                if (GameAchievements.hasAchievement(player, new ParkourAchievement()))
                    return;

                GameAchievements.giveAchievement(new ParkourAchievement(), player);
            }

        } else if (event.getClickedBlock().getType() == Material.IRON_PLATE) {
            Location plate = event.getClickedBlock().getLocation().clone();

            if (plate.subtract(0, 2, 0).getBlock().getType() == Material.IRON_BLOCK && plate.subtract(0, 1, 0).getBlock().getType() == Material.IRON_BLOCK) {
                player.teleport(Bukkit.getWorlds().get(0).getSpawnLocation());
                Bukkit.getScheduler().runTaskLater(ess, new Runnable() {
                    @Override
                    public void run() {
                        if (GameAchievements.hasAchievement(player, new ParkourAchievement()))
                            return;

                        GameAchievements.giveAchievement(new ParkourAchievement(), player);
                    }
                }, 2l);
            }

        }
    }
}
