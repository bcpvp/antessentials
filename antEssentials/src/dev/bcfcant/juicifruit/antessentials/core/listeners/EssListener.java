package dev.bcfcant.juicifruit.antessentials.core.listeners;

import org.bukkit.event.Listener;

import dev.bcfcant.juicifruit.antessentials.AntEssentials;

public abstract class EssListener implements Listener {

	protected AntEssentials ess;

	protected EssListener(AntEssentials ess) {
		this.ess = ess;
	}
}
