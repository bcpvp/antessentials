package dev.bcfcant.juicifruit.antessentials.core;

import java.util.Arrays;
import java.util.List;

import org.bukkit.plugin.Plugin;

import dev.bcfcant.juicifruit.antlib.api.AbstractConfigManager;
import dev.bcfcant.juicifruit.antlib.core.util.yaml.YAMLProcessor;

public class ConfigManager extends AbstractConfigManager {

	public int maxUserCacheCount;
	public int maxTempban;
	public List<String> socialSpyCommands;

	public ConfigManager(Plugin plugin, YAMLProcessor config) {
		super(plugin, config);
	}

	public void load() {
		super.load();

		config.setComment("max-user-cache-count", "Maximum number of users to cache on the server");
		long count = Runtime.getRuntime().maxMemory() / 1024L / 96L;
		maxUserCacheCount = config.getInt("max-user-cache-count", (int) count);

		config.setComment("max-tempban-time", "Maximum tempban time in seconds (0 for unlimited)");
		maxTempban = config.getInt("max-tempban-time", 0);

		config.setComment("social-spy-commands", "Private messaging commands");
		socialSpyCommands = config.getStringList("social-spy-commands", Arrays.asList("message", "msg", "m", "tell", "t", "whisper", "w"));
	}
}
